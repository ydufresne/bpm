#include <stdio.h>

#ifndef __BLOOM_H
#define __BLOOM_H

#define BLOOM_SIZE  8388593 /* Value in bits */
#define BLOOM_CHARS 1048575 /* Value in bytes */


typedef char * bitarray;

#define array_idx(val) ((val%BLOOM_SIZE) >> 3)
#define bit_idx(val) ((val%BLOOM_SIZE) & 0b111)

#define bf_init(array) (array = malloc(BLOOM_CHARS * sizeof(char)))
#define bf_add_bit(array, val) (array[array_idx(val)] |= 0b1 << bit_idx(val))
#define bf_contains(array, val) (array[array_idx(val)] & (0b1 << bit_idx(val)))


#define bf_write(array, fw) do { fwrite(array, sizeof(char), BLOOM_CHARS, fw); } while (1 == 0)

#define bf_read(array, file_reader) do { \
	if (!fread((char*)array, sizeof(char), BLOOM_CHARS, file_reader)) \
		fprintf(stderr, "Impossible to read the bloom filter\n"); \
} while (1 == 0)


#define bf_print(array) do { \
	for (uint _idx=0 ; _idx<BLOOM_CHARS ; _idx++) { \
		printf("%d%d%d%d %d%d%d%d ", \
			array[BLOOM_CHARS - _idx - 1] & 0b10000000 ? 1:0,\
			array[BLOOM_CHARS - _idx - 1] & 0b01000000 ? 1:0,\
			array[BLOOM_CHARS - _idx - 1] & 0b00100000 ? 1:0,\
			array[BLOOM_CHARS - _idx - 1] & 0b00010000 ? 1:0,\
			array[BLOOM_CHARS - _idx - 1] & 0b00001000 ? 1:0,\
			array[BLOOM_CHARS - _idx - 1] & 0b00000100 ? 1:0,\
			array[BLOOM_CHARS - _idx - 1] & 0b00000010 ? 1:0,\
			array[BLOOM_CHARS - _idx - 1] & 0b00000001 ? 1:0 \
		); \
		if (_idx % 8 == 7) \
			printf("\n"); \
	} \
	printf("\n"); \
} while (1 == 0)

#endif
