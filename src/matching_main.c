#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "wildcards.h"

#include "matching.h"


#define TRUE 42
#define FALSE !42


static char * binary_filename = NULL;
static char ** sequence_filenames = NULL;
static uint nb_filenames = 0;


/* --- Command treatment --- */
void matching_help() {
	printf("Command to lanch the software:\n");
	printf("./bin/matching -bin <binary.bin> -sequences <seqs.fasta>\n");
}

/* Load file variables and return true if everything is ok */
int parse_command_line(int argc, char * argv[]) {
	char * tmp_binary_filename = NULL;
	char * tmp_sequences_filename = NULL;


	/* --- Parse command line --- */
	for (uint idx=1 ; idx<argc ; idx++) {

		/* --- Arguments with values --- */
		if (idx == argc-1)
			break;

		/* If it's impossible to load value for an option */
		if (argv[idx + 1][0] == '-') {
			fprintf(stderr, "Option %s can't be immediatly followed by another option.\n", argv[idx]);
			return FALSE;
		}

		if (strcmp("-help", argv[idx]) == 0 || strcmp("-h", argv[idx]) == 0) {
			matching_help();
			return 1;
		} else if (strcmp("-bin", argv[idx]) == 0 || strcmp("-b", argv[idx]) == 0)
			tmp_binary_filename = argv[++idx];
		else if (strcmp("-sequences", argv[idx]) == 0 || strcmp("-s", argv[idx]) == 0)
			tmp_sequences_filename = argv[++idx];
	}

	/* Wrong command line */
	if (!tmp_binary_filename || !tmp_sequences_filename) {
		matching_help();
		return FALSE;
	}

	binary_filename = tmp_binary_filename;
	int ret_val = expandWildCards(tmp_sequences_filename, (const char ***)&sequence_filenames);

	switch(ret_val) {
		case 0:
			sequence_filenames = malloc(sizeof(char *));
			sequence_filenames[0] = malloc(strlen(tmp_sequences_filename) * sizeof(char) + 3);
			strcpy(sequence_filenames[0], tmp_sequences_filename);
			nb_filenames = 1;
			break;
		case -1:
			fprintf(stderr, "No file associated with the wildcard %s\n", tmp_sequences_filename);
			exit(4);
		default:
			nb_filenames = ret_val;
	}

	return TRUE;
}



int main (int argc, char * argv[]) {
	/* Parse the command line options */
	uint ret_val = parse_command_line(argc, argv);

	if (ret_val == 1)
		return 0;
	else if (!ret_val) 
		return 1;


	/* Trigger the main algorithm with correct parameters */
	char ** filenames = malloc((nb_filenames + 1) * sizeof(char *));
	filenames[0] = binary_filename;
	for (uint idx=0 ; idx<nb_filenames ; idx++)
		filenames[1+idx] = sequence_filenames[idx];
	free(sequence_filenames);

	if (!match(nb_filenames, filenames))
		return 1;

	free(filenames);

	return 0;
}




