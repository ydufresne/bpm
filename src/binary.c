#include <stdio.h>

#include "binary.h"


void save_as_binary (char * filename, seq_list_t * sequences, kmer_list_t * kmers, bitarray bloom) {
	FILE * fw = fopen(filename, "wb");

	seq_write_binary(sequences, fw);
	write_kmers(fw, kmers);
	bf_write(bloom, fw);

	fclose(fw);
}


binary_content * load_from_binary (char * filename) {
	binary_content * content = malloc(sizeof(binary_content));

	FILE * fr = fopen(filename, "rb");
	/* Load sequences */
	content->sequences = seq_read_binary(fr);

	/* Load kmers */
	content->kmers = load_kmers(fr, content->sequences->size * 2);

	/* Load bloom filter */
	bf_init(content->bloom);
	bf_read(content->bloom, fr);

	fclose(fr);

	return content;
}
