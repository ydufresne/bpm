#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "tommyhashdyn.h"
#include "hash_fasta.h"
#include "sequences.h"
#include "kmers.h"
#include "bloom.h"
#include "binary.h"

#define TRUE 42
#define FALSE !42

#define KMER_SIZE 32


static char * kmc_filename = NULL;



void hash_sequence(seq_t seq);
int hash_fasta();



/* Structure for the hash table */
typedef struct {
	tommy_hashdyn_node node;
	uint64_t hash;
	uint count;
} hashed_object_t;

static int compare_hashed (const void* arg, const void* obj) {
	return *(const uint64_t*)arg != ((hashed_object_t *)obj)->hash;
}


static uint hash_load_kmers (char * kmc_filename, tommy_hashdyn * table);
kmer_list_t * search_kmers(tommy_hashdyn * hashtable, seq_t * sequences, uint nb_seq);

bitarray create_bloom_filter(kmer_list_t * kmers);


int hash_fasta(char ** filenames) {
	// Name the files
	char * alleles_filename = filenames[0];
	char * kmc_filename = filenames[1];
	char * binary_filename = filenames[2];

	// Load sequences from fasta
	printf("Load sequences\n");
	seq_list_t * alleles = read_fasta (alleles_filename);
	printf("%u sequences loaded\n", alleles->size);

	printf("Init hashtable\n");
	// Load all the kmers
	tommy_hashdyn hashtable;
	tommy_hashdyn_init(&hashtable);
	printf("Load kmers\n");
	uint nb_kmers = hash_load_kmers(kmc_filename, &hashtable);
	printf("%u kmers loaded\n", nb_kmers);

	// Search for best kmer for each sequence
	printf("Search for the best kmer in each sequence\n");
	kmer_list_t * kmers = search_kmers(&hashtable, alleles->values, alleles->size);

	// Create a bloom filter for the alleles
	printf("Create a bloom filter for the kmers\n");
	bitarray array = create_bloom_filter(kmers);
	
	// Write the binary file
	save_as_binary(binary_filename, alleles, kmers, array);

	// Free the memory
	destroy_sequences(alleles);
	free(array);
	destroy_kmers(kmers);

	return TRUE;
}


#define complement_hash(hash) ((uint64_t)((~hash) & 0b11))
#define letter_hash(letter) ((uint64_t)((letter & 0b100) ? (0b10 | ((~letter) & 0b01)) : ((letter >> 1) & 0b11)))


static uint hash_load_kmers (char * kmc_filename, tommy_hashdyn * table) {
	uint nb_kmers = 0;

	FILE * fp = fopen(kmc_filename, "r");
	size_t buff_len = 1024;
	char * buffer = malloc(buff_len * sizeof(char));

	uint size;
	while ((size = getline(&buffer, &buff_len, fp)) != -1) {
		if (size < KMER_SIZE + 1)
			continue;

		// count
		uint count = atol(buffer + KMER_SIZE + 1);

		// fwd hash
		hashed_object_t * obj = malloc(sizeof(hashed_object_t));
		obj->hash = 0;

		for (uint idx=0 ; idx<KMER_SIZE ; idx++)
			obj->hash = (obj->hash << 2) | letter_hash(buffer[idx]);
		obj->count = count;
		tommy_hashdyn_insert(table, &(obj->node), obj, obj->hash);

		// rev hash
		obj = malloc(sizeof(hashed_object_t));
		obj->hash = 0;

		for (int idx=KMER_SIZE-1 ; idx>=0 ; idx--)
			obj->hash = (obj->hash << 2) | complement_hash(letter_hash(buffer[idx]));
		obj->count = count;
		tommy_hashdyn_insert(table, &(obj->node), obj, obj->hash);

		// Next kmer
		nb_kmers += 1;
	}

	free(buffer);
	fclose(fp);

	return nb_kmers;
}


kmer_list_t * search_kmers(tommy_hashdyn * hashtable, seq_t * sequences, uint nb_seq) {
	kmer_list_t * kmers = malloc(sizeof(kmer_list_t));
	kmers->values = malloc(2 * nb_seq * sizeof(kmer_t));
	kmers->size = nb_seq * 2;

	for (uint seq_idx=0 ; seq_idx<nb_seq ; seq_idx++) {
		seq_t * seq = sequences + seq_idx;

		// Prehash
		uint64_t h = 0;
		for (uint idx=0 ; idx<KMER_SIZE-1 ; idx++)
			h = h << 2 | letter_hash(seq_value(seq, idx));

		// Define minimums
		uint64_t best_hash = 0;
		uint min = 1000000;
		uint best_idx;

		// Hash and min found
		for (uint idx=KMER_SIZE-1 ; idx<seq->length ; idx++) {
			h = h << 2 | letter_hash(seq_value(seq, idx));
		
			hashed_object_t * obj = tommy_hashdyn_search(hashtable, compare_hashed, &h, h);
			if (!obj) {
				printf("Kmer not found at idx %u in seq %s\n", idx, seq_get_id(seq));
			} else {
				if (obj->count < min) {
					min = obj->count;
					best_hash = obj->hash;
					best_idx = idx - KMER_SIZE + 1;
				}
			}
		}

		// Register the forward kmer
		kmers->values[seq_idx*2].hash = best_hash;
		kmers->values[seq_idx*2].offset = best_idx;

		// Register the reverse kmer
		// The reverse idx correspond to the idx in the reverse complemented read
		kmers->values[seq_idx*2+1].offset = seq->length - best_idx - KMER_SIZE;
		kmers->values[seq_idx*2+1].hash = 0;
		for (uint idx=0 ; idx<KMER_SIZE ; idx++) {
			uint real_idx = best_idx + KMER_SIZE - 1 - idx;
			kmers->values[seq_idx*2+1].hash = kmers->values[seq_idx*2+1].hash << 2 | complement_hash(letter_hash(seq_value(seq,real_idx)));
		}
	}

	return kmers;
}

bitarray create_bloom_filter(kmer_list_t * kmers) {
	/* Iit the bloom filter with less than 8Mio.
	This size of data should stay in the L3 CPU cache.
	*/
	bitarray array;
	bf_init(array);

	/* Fill the bloom filter with all the kmers */
	for (uint idx=0 ; idx<kmers->size ; idx++) {
		bf_add_bit(array, kmers->values[idx].hash);
	}

	// bf_print(array);

	return array;
}
