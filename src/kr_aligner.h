#include <stdlib.h>

#include "kmers.h"
#include "sequences.h"
#include "bloom.h"


int kr_init (kmer_list_t * kmers, seq_list_t * sequences, bitarray bloom_filter);
int kr_align (seq_t * seq, FILE * output);
void kr_destroy();


uint64_t * hash(char * text, size_t length);
void hash_decal (uint64_t * hashes, char letter);
