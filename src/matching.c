#include <stdio.h>
#include <string.h>

#include "matching.h"
#include "sequences.h"
#include "kmers.h"
#include "kr_aligner.h"
#include "binary.h"



/* --- Algorithm part --- */

int match (uint nb_sequence_files, char ** filenames) {
	/* define filname variables */
	char * binary_filename = filenames[0];
	char ** sequence_filenames = filenames + 1;

	/* --- Loading binary file --- */
	binary_content * content = load_from_binary (binary_filename);

	seq_list_t * alleles = content->sequences;
	kmer_list_t * kmers = content->kmers;
	bitarray bloom_filter = content->bloom;

	free(content);
	
	/* --- Perform alignment --- */
	/* Init hash tables */
	if (!kr_init(kmers, alleles, bloom_filter)) {
		fprintf(stderr, "Impossible to save kmers in hash table\n");
		return 0;
	}


	/* --- Read sequences from FASTA and find the locations of alleles */
	for (uint file_idx=0 ; file_idx<nb_sequence_files ; file_idx++) {
		seq_list_t * sequences = read_fasta(sequence_filenames[file_idx]);

		for (uint idx=0 ; idx<sequences->size ; idx++) {
			kr_align (sequences->values + idx, stdout);
		}
		
		destroy_sequences(sequences);
	}

	kr_destroy();
	destroy_sequences(alleles);
	destroy_kmers(kmers);
	free(bloom_filter);

	return 1;
}

