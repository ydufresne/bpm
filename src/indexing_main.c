#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "hash_fasta.h"

#define TRUE 42
#define FALSE !42


void index_help() {
	printf("Command to lanch the software:\n");
	printf("./bin/index -kmc <alleles.kmc> -alleles <alleles.fasta> -bin <binary.bin>\n");
	printf("The kmc file is created using the kmc softwares. Plese see the github to know how to generate this file.\n");
}

/* --- Define filenames --- */
char * alleles_filename = NULL;
char * kmc_filename = NULL;
char * binary_filename = NULL;


/* Load file variables and return true if everything is ok */
int parse_command_line(int argc, char * argv[]) {
	char * tmp_alleles_filename = NULL;
	char * tmp_binary_filename = NULL;
	char * tmp_kmc_filename = NULL;


	/* --- Parse command line --- */
	for (uint idx=1 ; idx<argc ; idx++) {

		/* --- Arguments with values --- */
		if (idx == argc-1)
			break;

		/* If it's impossible to load value for an option */
		if (argv[idx + 1][0] == '-') {
			fprintf(stderr, "Option %s can't be immediatly followed by another option.\n", argv[idx]);
			return FALSE;
		}

		if (strcmp("-alleles", argv[idx]) == 0 || strcmp("-a", argv[idx]) == 0)
			tmp_alleles_filename = argv[++idx];
		else if (strcmp("-help", argv[idx]) == 0 || strcmp("-h", argv[idx]) == 0) {
			index_help();
			return 1;
		} else if (strcmp("-kmc", argv[idx]) == 0 || strcmp("-k", argv[idx]) == 0)
			tmp_kmc_filename = argv[++idx];
		else if (strcmp("-bin", argv[idx]) == 0 || strcmp("-b", argv[idx]) == 0)
			tmp_binary_filename = argv[++idx];
	}

	/* Wrong command line */
	if (!tmp_alleles_filename || !tmp_binary_filename) {
		index_help();
		return FALSE;
	}

	alleles_filename = tmp_alleles_filename;
	binary_filename = tmp_binary_filename;
	kmc_filename = tmp_kmc_filename;

	return TRUE;
}


int main (int argc, char * argv[]) {
	/* Argument parsing */
	uint ret_val = parse_command_line(argc, argv);

	/* Case of help */
	if (ret_val == 1)
		return 0;
	else if (!ret_val)
		return 1;

	/* Filname compressing */
	char * filenames[3];
	filenames[0] = alleles_filename; filenames[1] = kmc_filename; filenames[2] = binary_filename;
	
	/* kmer hashing */
	if (!hash_fasta(filenames))
		return 1;

	return 0;
}
