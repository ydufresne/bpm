#include <stdio.h>
#include <stdlib.h>


#ifndef __FASTA_H
#define __FASTA_H


#define seq_value(pseq, idx) (pseq->buffer[pseq->offset + idx])
#define seq_get_id(pseq) (pseq->buffer + pseq->id_offset)


typedef struct {
	size_t size;
	uint next_free;
	char * array;
} buffer_t;


typedef struct {
	char * buffer;
	uint offset;
	uint id_offset;
	uint length;
} seq_t;

typedef struct {
	seq_t * values;
	uint size;
	/* Buffer used to store all the strings.
	You don't have to read or modify it.
	This attribute will be use to deallocate all the structures. */
	buffer_t * buffer;
} seq_list_t;

seq_list_t * read_fasta (char *);
seq_list_t * seq_read_binary(FILE *);
void seq_write_binary(seq_list_t *, FILE *);
void destroy_sequences(seq_list_t *);

#endif
