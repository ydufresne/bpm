
#include "kmers.h"
#include "sequences.h"
#include "bloom.h"


typedef struct {
	seq_list_t * sequences;
	kmer_list_t * kmers;
	bitarray bloom;
} binary_content;


void save_as_binary (char *, seq_list_t *, kmer_list_t *, bitarray);
binary_content * load_from_binary (char * filename);
