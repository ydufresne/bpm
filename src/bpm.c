#include <string.h>
#include <stdio.h>
#include <stdlib.h>


void help(FILE * output) {
	fprintf(output, "   bpm [ index | match ] [ -help | index or match arguments ]\n");
	fprintf(output, "To list indexing (resp. matching) arguments please type:\n");
	fprintf(output, "   bpm index -help\n");
}

char * command_line(int argc, char * argv[]) {
	/* Count size for the malloc of the new command */
	uint size = 0;
	for (uint idx=0 ; idx<argc ; idx++)
		size += strlen(argv[idx]);
	char * command = malloc(sizeof(char) * size + 20);

	/* construct command line */
	command[0] = '\0';
	/* Select the path */
	char * slash = strrchr(argv[0], '/');
	if (slash != NULL) {
		slash[1] = '\0';
		strcpy(command, argv[0]);
	}
	/* Add the program name */
	strcat(command, argv[1]);

	/* Concat command arguments */
	for (uint idx=2 ; idx<argc ; idx++) {
		strcat(command, " ");
		strcat(command, argv[idx]);
	}

	return command;
}


int main (int argc, char * argv[]) {
	/* Verify number of arguments */
	if (argc == 1) {
		fprintf(stderr, "The command line should at least contains the program ");
		fprintf(stderr, "to execute and the linked parameters.\n");
		help(stderr);
		return 1;
	}

	if (strcmp(argv[1], "index") == 0 || strcmp(argv[1], "match") == 0) {
		char * cmd = command_line(argc, argv);
		int cmd_ret = system(cmd);
		free(cmd);

		return cmd_ret;
	} else if (strcmp(argv[1], "-help") == 0 || strcmp(argv[1], "-h") == 0) {
		help(stdout);
	} else {
		fprintf(stderr, "Wrong parameters\n");
		help(stderr);
		return 1;
	}
	
	return 0;
}
