#include <string.h>
#include <zlib.h>
#include <stdint.h>

#include "sequences.h"
#include "kseq.h"

KSEQ_INIT(gzFile, gzread)


/** Create a huge buffer to allocate all the strings inside
  * The buffer can't be overflowed because the sequences in the file are smaller
  * than the file itself
  */
buffer_t * init_buffer (char * filename) {
	buffer_t * buff = malloc (sizeof(buffer_t) * sizeof(char));

	// File size
	FILE * fp = fopen(filename, "r");
	fseek(fp, 0L, SEEK_END);
	buff->size = ftell(fp);
	fclose(fp);

	// Data array
	buff->array = malloc(buff->size * sizeof(char));
	buff->next_free = 0;

	return buff;
}

/** Detroy the buffer and the structure around it */
void destroy_sequences (seq_list_t * seqs) {
	free(seqs->buffer->array);
	free(seqs->buffer);
	free(seqs->values);
	free(seqs);
}

/** Read all the sequences in the file using the zlib and the kseq lib.
  * All the sequences are copied in the buffer and addresses are copied in the seq_t structs
  */
seq_list_t * read_fasta (char * filename) {
	/* Init memory to store all the sequences in a contiguous memory */
	seq_list_t * sequences = malloc(sizeof(seq_list_t));
	sequences->buffer = init_buffer(filename);
	buffer_t * buff = sequences->buffer;

	// Number of sequences
	uint nb_sequences = 0;
	uint malloced_seqt = 1024;

	// Pointer to sequences
	seq_t * values = malloc(malloced_seqt * sizeof(seq_t));

	// prepare the kseq reader
	gzFile fp = gzopen(filename, "r");
	kseq_t *seq = kseq_init(fp);
	int l;

	// Read all the sequences
	while ((l = kseq_read(seq)) >= 0) {
		// Realloc in case of too few sequences poiters
		if (nb_sequences == malloced_seqt) {
			malloced_seqt *= 2;
			values = realloc(values, malloced_seqt * sizeof(seq_t));
		}

		// Set the address of the header in the buffer
		values[nb_sequences].id_offset = buff->next_free;
		// move the header
		buff->array[buff->next_free] = '\0';
		strcat(buff->array + buff->next_free, seq->name.s);
		buff->next_free += seq->name.l;
		//add the comment
		if (seq->comment.l) {
			// Add a space before the comment
			buff->array[buff->next_free] = ' ';
			buff->next_free += 1;
			buff->array[buff->next_free] = '\0';
			// copy the comment
			strcat(buff->array + buff->next_free, seq->comment.s);
			buff->next_free += seq->comment.l;
		}
		// Add the space to keep the last \0
		buff->next_free += 1;

		values[nb_sequences].buffer = buff->array;
		values[nb_sequences].offset = buff->next_free;
		values[nb_sequences].length = seq->seq.l;
		// move the sequence
		buff->array[buff->next_free] = '\0';
		strcat(buff->array + buff->next_free, seq->seq.s);
		buff->next_free += seq->seq.l + 1;

		// increase the sequence number
		nb_sequences += 1;
	}

	//free all the zlib and kseq structs
	kseq_destroy(seq);
	gzclose(fp);

	/* Transform brut values to list struct */
	sequences->size = nb_sequences;
	sequences->values = values;

	return sequences;
}


seq_list_t * seq_read_binary(FILE * fr) {
	seq_list_t * sequences = malloc(sizeof(seq_list_t));
	/* Read the number of sequences and allocate corresponding memory */
	if (!fread(&(sequences->size), sizeof(uint32_t), 1, fr)) {
		fprintf(stderr, "Impossible to read the number of alleles in the binary file\n");
		exit(1);
	}
	sequences->values = malloc(sizeof(seq_t) * sequences->size);

	/* Read the list of sequences */
	if (!fread(sequences->values, sizeof(seq_t), sequences->size, fr)) {
		fprintf(stderr, "Impossible to load the sequences structures from the binary file\n");
		exit(1);
	}

	/* Read the size of the string buffer */
	sequences->buffer = malloc(sizeof(buffer_t));
	if (!fread(&(sequences->buffer->size), sizeof(uint64_t), 1, fr)) {
		fprintf(stderr, "Impossivle to load the buffer size from the binary file\n");
		exit(1);
	}

	/* Read the string buffer */
	sequences->buffer->array = malloc(sequences->buffer->size * sizeof(char));
	sequences->buffer->next_free = sequences->buffer->size;
	if (!fread(sequences->buffer->array, sizeof(char), sequences->buffer->size, fr)) {
		fprintf(stderr, "Impossivle to load the buffer size from the binary file\n");
		exit(1);
	}

	/* Correct the pointers to the buffer */
	for (uint idx=0 ; idx<sequences->size ; idx++) {
		sequences->values[idx].buffer = sequences->buffer->array;
	}

	return sequences;
}


void seq_write_binary(seq_list_t * sequences, FILE * fw) {
	/* Write the number of sequences */
	uint32_t nb_sequences = sequences->size;
	fwrite(&nb_sequences, sizeof(uint32_t), 1, fw);

	/* Write the sequence structures */
	fwrite(sequences->values, sizeof(seq_t), sequences->size, fw);

	/* Write the buffer size */
	uint64_t buff_size = sequences->buffer->next_free;
	fwrite(&buff_size, sizeof(uint64_t), 1, fw);

	/* Write the buffer */
	fwrite(sequences->buffer->array, sizeof(char), buff_size, fw);
}
