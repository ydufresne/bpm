#include <stdlib.h>
#include "kmers.h"

#define TRUE 42
#define FALSE !42


static char kmer_init = FALSE;
static char hash_size = 0;
static char offset_size = 0;


void are_sizes_correct(FILE * fp) {
	hash_size = fgetc(fp);
	offset_size = fgetc(fp);

	if (hash_size != sizeof(uint64_t)) {
		fprintf(stderr, "Please use a hash size of %lu bytes\n", sizeof(uint64_t));
		exit(1);
	}
	if (offset_size != sizeof(uint32_t)) {
		fprintf(stderr, "Please use a hash size of %lu bytes\n", sizeof(uint32_t));
		exit(1);
	}
	kmer_init = TRUE;
}


kmer_list_t * load_kmers(FILE * fr, uint nb_kmers) {
	/* Init kmer structures */
	kmer_list_t * kmers = malloc(sizeof(kmer_list_t));
	kmers->values = malloc(sizeof(kmer_t) * nb_kmers);
	kmers->size = nb_kmers;

	/* Verify the data sizes */
	are_sizes_correct(fr);

	/* Fill the values */
	if (!fread(kmers->values, sizeof(kmer_t), nb_kmers, fr)) {
		fprintf(stderr, "Impossible to read kmers in the binary file\n");
		exit(1);
	}

	return kmers;
}

void write_kmers(FILE * fw, kmer_list_t * kmers) {
	char buff[2];
	buff[0] = sizeof(uint64_t);
	buff[1] = sizeof(uint32_t);
	/* Write header */
	fwrite(buff, sizeof(char), 2, fw);

	/* Write kmers */
	fwrite(kmers->values, sizeof(kmer_t), kmers->size, fw);
}

void destroy_kmers (kmer_list_t * kmers) {
	free(kmers->values);
	free(kmers);
}
