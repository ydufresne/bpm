#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>


#ifndef __KMERS_H
#define __KMERS_H


typedef struct kmer_s {
	uint64_t hash;
	uint32_t offset;
} __attribute__((packed)) kmer_t;

typedef struct {
	uint size;
	kmer_t * values;
} kmer_list_t;


// void next_kmers(FILE *, kmer_t *, size_t);
kmer_list_t * load_kmers(FILE * fr, uint nb_kmers);
void write_kmers(FILE * fw, kmer_list_t * kmers);
void destroy_kmers(kmer_list_t * kmers);


#endif
