#include <string.h>

#include "tommyhashdyn.h"

#include "kr_aligner.h"


#define HASH_SIZE 32
#define TRUE 42
#define FALSE !42


/* Structure for the hash table */
typedef struct {
	tommy_hashdyn_node node;
	kmer_t * kmers;
	seq_t * seq;
} hashed_object_t;



/* Hashtable to store all the reads and main kmers regarding the hashed value of the kmer */
static tommy_hashdyn hashtable;
static bitarray bf;

/* Init the data structure (hashtable) for future searches
 * Create one entry in the hashtable table
 */
int kr_init (kmer_list_t * kmers, seq_list_t * sequences, bitarray bloom_filter) {
	/* Load kmers in hashtable */
	tommy_hashdyn_init(&hashtable);

	for (uint idx=0 ; idx<kmers->size ; idx++) {
		/* Try one insertion */
		hashed_object_t * object = malloc(sizeof(hashed_object_t));
		object->kmers = kmers->values + idx - (idx % 2);
		object->seq = sequences->values + idx/2;

		tommy_hashdyn_insert(&hashtable, &(object->node), object, object->kmers[idx%2].hash);
	}

	// for (uint idx=0 ; idx<sequences->size ; idx++) {
	// 	/* Try one insertion */
	// 	hashed_object_t * object = malloc(sizeof(hashed_object_t));
	// 	object->kmers = kmers->values + idx*2;
	// 	object->seq = sequences->values + idx;

	// 	tommy_hashdyn_insert(&hashtable, &(object->node), object, object->kmers[0].hash);

	// 	object = malloc(sizeof(hashed_object_t));
	// 	object->kmers = kmers->values + idx*2;
	// 	object->seq = sequences->values + idx;

	// 	tommy_hashdyn_insert(&hashtable, &(object->node), object, object->kmers[1].hash);
	// }

	bf = bloom_filter;

	return TRUE;
}


/* Declaration of hash functions */
char complement(char);
int verify_sequence (seq_t * ref, uint seq_offset, hashed_object_t * obj);
int verify_revert_sequence (seq_t * ref, uint seq_offset, hashed_object_t * obj);

typedef struct {
	int offset;
	seq_t * seq;
	char direction;
} result_t;


static result_t * results = NULL;
static uint buffered_results = 0;

uint search_hash (uint64_t h, seq_t * ref, uint seq_offset) {
	/* Bloom filter application */
	if (!bf_contains(bf, h))
		return 0;

	/* search for the hash value */
	tommy_hashdyn_node * node = tommy_hashdyn_bucket(&hashtable, h);

	// Init
	uint nb_results = 0;

	while (node) {
		hashed_object_t * obj = node->data;

		// Get the correct verification function
		int (*verification)(seq_t *, uint, hashed_object_t *);
		verification = NULL;
		uint offset = 0;
		char direction = '~';

		if (h == obj->kmers[0].hash) {
			verification = verify_sequence;
			offset = seq_offset-obj->kmers[0].offset;
			direction = '+';
		} else if (h == obj->kmers[1].hash) {
			verification = verify_revert_sequence;
			offset = seq_offset - obj->seq->length + obj->kmers[1].offset + HASH_SIZE;
			direction = '-';
		}

		if (verification && verification(ref, seq_offset, obj)) {
			/* realloc if needed */
			if (nb_results >= buffered_results) {
				results = realloc(results, (nb_results+1) * sizeof(result_t));
				buffered_results++;
			}

			/* Create the result */
			results[nb_results].offset = offset;
			results[nb_results].seq = obj->seq;
			results[nb_results].direction = direction;

			nb_results++;
		}

		node = node->next;
	}

	return nb_results;
}

int verify_sequence (seq_t * ref, uint seq_offset, hashed_object_t * obj) {
	/* Verify if the searched pattern can fit into the reference */
	uint start_idx = seq_offset - obj->kmers[0].offset;
	uint end_idx = start_idx + obj->seq->length - 1;

	if (obj->kmers[0].offset > seq_offset || end_idx >= ref->length)
		return FALSE;

	/* Verify before the kmer */
	for (uint idx=0, sidx=start_idx ; idx<obj->kmers[0].offset ; idx++, sidx++)
		if (seq_value(ref, sidx) != seq_value(obj->seq, idx))
			return FALSE;

	/* verify after the kmer */
	for (uint idx=obj->kmers[0].offset+HASH_SIZE, sidx=seq_offset+HASH_SIZE ; idx<obj->seq->length ; idx++, sidx++)
		if (seq_value(ref, sidx) != seq_value(obj->seq, idx))
			return FALSE;

	return TRUE;
}

int verify_revert_sequence (seq_t * ref, uint seq_offset, hashed_object_t * obj) {
	seq_t * allele = obj->seq;
	kmer_t * kmer = obj->kmers + 1;
	
	// verify before
	for (uint seq_idx=seq_offset-kmer->offset, all_idx=allele->length-1 ; seq_idx<seq_offset ; seq_idx++, all_idx--) {
		if (complement(seq_value(ref, seq_idx)) != seq_value(allele, all_idx))
			return FALSE;
	}

	// verify after
	for (int seq_idx=seq_offset+HASH_SIZE, all_idx=obj->kmers->offset-1 ; all_idx>=0 ; seq_idx++, all_idx--) {
		if (complement(seq_value(ref, seq_idx)) != seq_value(allele, all_idx))
			return FALSE;
	}

	return TRUE;
}

// Hack on ascii code to get hashed letter
#define letter_hash(letter) ((uint64_t)((letter & 0b100) ? (0b10 | ((~letter) & 0b01)) : ((letter >> 1) & 0b11)))
#define hash_decal(h, letter) ((h << 2) | letter_hash(letter))


int kr_align (seq_t * seq, FILE * output) {
	
	/* First hash */
	uint64_t h = 0;
	for (uint idx=0 ; idx<HASH_SIZE-1 ; idx++)
		h = hash_decal(h, seq_value(seq, idx));

	/* --- Hash recurence --- */
	for (uint idx=0 ; idx<seq->length - HASH_SIZE ; idx++) {
		h = hash_decal(h, seq_value(seq, idx+HASH_SIZE-1));
		// --- Search forward hash ---
		uint nb_results = search_hash(h, seq, idx);
		// Interpret search result
		for (uint res_idx=0 ; res_idx<nb_results ; res_idx++) {
			fprintf(output, "%s\t%s\t%c\t%u\n", seq_get_id(seq), seq_get_id(results[res_idx].seq), results[res_idx].direction, results[res_idx].offset);
		}
	}


	return TRUE;
}


/* --- Hashing functions --- */

char complement(char letter) {
	switch(letter) {
		case 'a':
		case 'A':
			return 'T';
		case 'c':
		case 'C':
			return 'G';
		case 'g':
		case 'G':
			return 'C';
		case 't':
		case 'T':
			return 'A';

		default:
			fprintf(stderr, "Impossible to complement %c. Replacing the complement by T.\n", letter);
			return 'T';
	}
}



void kr_destroy() {
	free(results);

	/* Free the hashtable and its elements */
	tommy_hashdyn_foreach(&hashtable, free);
	tommy_hashdyn_done(&hashtable);
}



