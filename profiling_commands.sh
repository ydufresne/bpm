make clean
cmake -DCMAKE_CXX_FLAGS=-pg -DCMAKE_C_FLAGS=-pg .
make VERBOSE=1
./bin/match -b data/alleles.bin -s data/SB*.fasta > /dev/null
gprof bin/match gmon.out > profile.txt
rm gmon.out
make clean
cmake -DCMAKE_CXX_FLAGS= -DCMAKE_C_FLAGS= .
make
