#include <stdio.h>

#include "minunit.h"
#include "fasta_tests.h"
#include "kr_tests.h"
#include "arguments_tests.h"
#include "kmer_tests.h"
#include "binary_tests.h"


int tests_run = 0;
char* result;

int main(int argc, char **argv) {
	printf("\n");

	// tests_run = 0;
	// result = arguments_tests();
	// summary();

	tests_run = 0;
	result = fasta_tests();
	summary();

	tests_run = 0;
	result = binary_tests();
	summary();

	tests_run = 0;
	result = kmer_tests();
	summary();

	tests_run = 0;
	result = rabinkarp_tests();
	summary();

	return 0;
}

