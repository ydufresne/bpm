#include <stdio.h>
/* Code from MinUnit
 * Code provided by http://www.jera.com/techinfo/jtns/jtn002.html
 * LICENCE (from the website): You may use the code in this tech note
 * for any purpose, with the understanding that it comes with NO WARRANTY. 
 */

#define mu_assert(message, test) do { if (!(test)) {return message;} } while (0)
#define mu_run_test(test) do { char *message = test(); tests_run++; \
                                if (message) return message; } while (0)
#define summary() do {if (result != 0) {printf("FAILED! %s\n\n", result);}\
						else {printf("Tests run: %d\n", tests_run);printf("ALL TESTS PASSED\n\n");}} while(0)
extern int tests_run;
extern char* result;
