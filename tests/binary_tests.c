#include <stdio.h>

#include "minunit.h"
#include "sequences.h"
#include "hash_fasta.h"
#include "binary.h"

static char * test_saving_loading() {
	/* Filenames */
	char * filenames[3];
	char * fasta_filename = "tests/data/data2.fasta";
	char * kmc_filename = "tests/data/data2.kmc";
	char * binary_filename = "tests/data/tmp_binary.bin";
	filenames[0] = fasta_filename;
	filenames[1] = kmc_filename;
	filenames[2] = binary_filename;

	/* Loading */
	seq_list_t * alleles = read_fasta(fasta_filename);

	/* Hashing for kmers and bloom filter */
	hash_fasta(filenames);

	/* Binary loading */
	binary_content * content = load_from_binary (binary_filename);

	return "TODO";
}



char * binary_tests() {
	printf("Binary tests\n");

	mu_run_test(test_saving_loading);
	return 0;
}
