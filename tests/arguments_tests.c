#include <stdio.h>

#include "minunit.h"

#include "matching.h"


// static char * test_command_line_without_arguments() {
// 	char * command_0[1] = {"./bin/main"};
// 	mu_assert("Should be impossible to run without arguments", !bpm_parse_command_line(1, command_0));

// 	return 0;
// }

// static char * test_command_line_with_corect_arguments() {
// 	char * command_0[] = {"./bin/main", "-alleles", "fake_alleles.fasta", "-kmers", "fake_kmers.bin",
// 						"-sequences", "fake_sequences.fasta"};
// 	mu_assert("Should work : -a -k -s", bpm_parse_command_line(7, command_0));

// 	char * command_1[] = {"./bin/main", "-kmers", "fake_kmers.bin", "-alleles", "fake_alleles.fasta",
// 						"-sequences", "fake_sequences.fasta"};
// 	mu_assert("Should work : -k -a -s", bpm_parse_command_line(7, command_1));

// 	char * command_2[] = {"./bin/main", "-kmers", "fake_kmers.bin",
// 						"-sequences", "fake_sequences.fasta", "-alleles", "fake_alleles.fasta"};
// 	mu_assert("Should work : -k -s -a", bpm_parse_command_line(7, command_2));

// 	return 0;
// }

// static char * test_incomplete_command_line() {
// 	char * command_0[] = {"./bin/main", "-alleles", "fake_alleles.fasta", "-kmers", "fake_kmers.bin"};
// 	mu_assert("Should not work : -a -k", !bpm_parse_command_line(5, command_0));

// 	char * command_1[] = {"./bin/main", "-alleles", "fake_alleles.fasta", "-sequences", "fake_sequences.fasta"};
// 	mu_assert("Should work : -a -s", !bpm_parse_command_line(5, command_1));

// 	char * command_2[] = {"./bin/main", "-kmers", "fake_kmers.bin",	"-sequences", "fake_sequences.fasta"};
// 	mu_assert("Should work : -k -s", !bpm_parse_command_line(5, command_2));

// 	return 0;
// }


// static char * test_wrong_command_line() {
// 	char * command_0[] = {"./bin/main", "-alleles", "-kmers", "fake_kmers.bin",
// 						"-sequences", "fake_sequences.fasta"};
// 	mu_assert("Should not work : -a is not followed by filename", !bpm_parse_command_line(6, command_0));

// 	char * command_1[] = {"./bin/main", "-alleles", "fake_alleles.fasta", "-kmers", "fake_kmers.bin",
// 						"-sequences"};
// 	mu_assert("Should work : -a -k -s", !bpm_parse_command_line(6, command_1));

// 	return 0;
// }


// char * arguments_tests() {
// 	printf("Command line tests\n");
// 	mu_run_test(test_command_line_without_arguments);
// 	mu_run_test(test_command_line_with_corect_arguments);
// 	mu_run_test(test_incomplete_command_line);
// 	mu_run_test(test_wrong_command_line);

// 	return 0;
// }
