#include "minunit.h"

#include <stdio.h>
#include <string.h>

#include "kr_aligner.h"
#include "kmers.h"
#include "sequences.h"
#include "tommyhashdyn.h"
#include "binary.h"


static seq_list_t * alleles;
static kmer_list_t * kmers;
static bitarray bloom;


/* --- Hashtable definition --- */
/* Structure for the hash table */
typedef struct {
	tommy_hashdyn_node node;
	kmer_t * kmer;
	seq_t * seq;
} hashed_object_t;


/* Hashtable to store all the reads and main kmers regarding the hashed value of the kmer */
static tommy_hashdyn hashtable;

/* --- End hashtable --- */

char * hash_test (uint64_t hash) {
	/* search for the hash value */
	tommy_node* i = tommy_hashdyn_bucket(&hashtable, hash);
	/* Look for possible collided values */
	while (i) {
		hashed_object_t * obj = i->data;
		if (hash == obj->kmer->hash) {
			return 0;
		}

		i = i->next;
	}
	fprintf(stderr, "Hashcode %lu not retreive in the hashtable\n", hash);
	return "Impossible to retreive hash.";
}

char * hashtable_filling_test () {
	/* --- Init --- */
	/* Load kmers in hashtable */
	tommy_hashdyn_init(&hashtable);

	kmer_t fake;
	fake.hash = 42;

	for (uint idx=0 ; idx<3 ; idx++) {
		/* Try one insertion */
		hashed_object_t * object = malloc(sizeof(hashed_object_t));
		object->kmer = kmers->values + idx;
		object->seq = alleles->values + idx;

		tommy_hashdyn_insert(&hashtable, &object->node, object, object->kmer->hash);
	}

	/* Add 100000 fake structs */
	for (uint idx=0 ; idx<100000 ; idx++) {
		hashed_object_t * object = malloc(sizeof(hashed_object_t));
		object->kmer = &fake;
		tommy_hashdyn_insert(&hashtable, &object->node, object, idx);
	}


	/* --- Test --- */
	uint nb_elements = tommy_hashdyn_count(&hashtable);
	mu_assert("The hashtable should contain 3 elements", nb_elements == 100003);

	char * msg;
	for (uint idx=0 ; idx<3 ; idx++) {
		msg = hash_test(kmers->values[idx].hash);
		if (msg != 0)
			return msg;
	}


	/* --- free memory --- */
	tommy_hashdyn_foreach(&hashtable, free);
	tommy_hashdyn_done(&hashtable);

	return 0;
}


char * fasta_against_itself_test () {
	char * tmp_file = "./tests/data/tmp.txt";

	FILE * tmp = fopen(tmp_file, "w");
	mu_assert("Impossible to execute a kr alignment with the sequence 0", kr_align(alleles->values, tmp));
	mu_assert("Impossible to execute a kr alignment with the sequence 1", kr_align(alleles->values + 1, tmp));
	mu_assert("Impossible to execute a kr alignment with the sequence 2", kr_align(alleles->values + 2, tmp));
	fclose(tmp);

	tmp = fopen(tmp_file, "r");
	size_t buff_size = 1024;
	char * buffer = malloc(buff_size * sizeof(char));

	ssize_t line_size;

	line_size = getline(&buffer, &buff_size, tmp);
	mu_assert("test allele 0 not found at position 0", strcmp(buffer, "rbsC_S_1\trbsC_S_1\t+\t0\n") == 0);
	line_size = getline(&buffer, &buff_size, tmp);
	mu_assert("test allele 1 not found at position 0", strcmp(buffer, "rbsC_S_2\trbsC_S_2\t+\t0\n") == 0);
	line_size = getline(&buffer, &buff_size, tmp);
	mu_assert("test allele 2 not found at position 0", strcmp(buffer, "rbsC_S_3\trbsC_S_3\t+\t0\n") == 0);
	mu_assert("Too much matches on data2.fasta", getline(&buffer, &buff_size, tmp) != 0);

	free(buffer);
	fclose(tmp);
	// remove(tmp_file);

	return 0;
}


char * fasta_against_sequence_with_overlap_and_rev_comp_test () {
	char * tmp_file = "./tests/data/tmp.txt";
	FILE * tmp = fopen(tmp_file, "w");

	char * sequences_filename = "tests/data/test_seq_overlapp.fasta";
	seq_list_t * sequences = read_fasta(sequences_filename);

	mu_assert("Impossible to execute a kr alignment with the sequence 0", kr_align(sequences->values, tmp));
	destroy_sequences(sequences);
	fclose(tmp);

	tmp = fopen(tmp_file, "r");
	size_t buff_size = 1024;
	char * buffer = malloc(buff_size * sizeof(char));

	int lines = getline(&buffer, &buff_size, tmp);
	mu_assert("0 lines in output", lines != -1);
	mu_assert("test allele 2 not found at position 964", strcmp(buffer, "Test_overlapping Must find allele 2x2 but not the 1 and 3 (1 nucleotide missing)\trbsC_S_2\t+\t964\n") == 0);
	mu_assert("not enougth lines in output", getline(&buffer, &buff_size, tmp) != -1);
	mu_assert("test allele 2 not found at position 1929", strcmp(buffer, "Test_overlapping Must find allele 2x2 but not the 1 and 3 (1 nucleotide missing)\trbsC_S_2\t+\t1929\n") == 0);
	mu_assert("not enougth lines in output", getline(&buffer, &buff_size, tmp) != -1);
	mu_assert("test allele 3 not found at position 2895", strcmp(buffer, "Test_overlapping Must find allele 2x2 but not the 1 and 3 (1 nucleotide missing)\trbsC_S_3\t-\t3829\n") == 0);
	mu_assert("Too much matches on test_seq_overlapp.fasta", getline(&buffer, &buff_size, tmp) != 0);

	free(buffer);
	fclose(tmp);
	remove(tmp_file);

	return 0;
}


char * init_hashtable () {
	mu_assert("Impossible to init the hashtable", kr_init (kmers, alleles, bloom));

	return 0;
}


char * rabinkarp_tests() {
	printf("Rabin-Karp tests\n");

	/* --- init sequences --- */
	char filename[] = "tests/data/data2.fasta";
	alleles = read_fasta(filename);

	/* ---	 init kmers --- */
	binary_content * content = load_from_binary("tests/data/data2.bin");
	kmers = content->kmers;
	bloom = content->bloom;


	/* --- Tests --- */
	mu_run_test(hashtable_filling_test);
	mu_run_test(init_hashtable);
	mu_run_test(fasta_against_itself_test);
	mu_run_test(fasta_against_sequence_with_overlap_and_rev_comp_test);


	/* --- Unload memory --- */
	destroy_sequences(alleles);
	destroy_kmers(kmers);
	free(bloom);
	free(content);

	return 0;
}
