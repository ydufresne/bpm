#include <stdio.h>
#include <string.h>

#include "minunit.h"
#include "sequences.h"


static char * test_file_without_endline() {
	seq_t * seq;

	char * filename = "tests/data/data.fasta";
	seq_list_t * sequences = read_fasta(filename);

	mu_assert("nb sequences read from data.fasta != 3", sequences->size == 3);

	destroy_sequences(sequences);

	return 0;
}

static char * test_file_with_endline() {
	seq_t seq;

	char * filename = "tests/data/data2.fasta";
	seq_list_t * sequences = read_fasta(filename);

	mu_assert("nb sequences read from data2.fasta != 3", sequences->size == 3);

	destroy_sequences(sequences);

	return 0;
}

static char * test_values_from_fasta() {
	char * filename = "tests/data/data.fasta";

	seq_list_t * sequences = read_fasta(filename);

	/* Sequence 1 */
	seq_t * seq = sequences->values;
	mu_assert("wrong id for rbsC_S_1", !strcmp(seq_get_id(seq), "rbsC_S_1"));
	mu_assert(
		"wrong sequence for rbsC_S_1",
		!strcmp(seq->buffer + seq->offset, "ATGACAACCCAGAATCGCTGATCAGTAA")
	);

	/* Sequence 2 */
	seq = sequences->values + 1;
	mu_assert("wrong id for rbsC_S_2", !strcmp(seq_get_id(seq), "rbsC_S_2"));
	mu_assert(
		"wrong sequence for rbsC_S_2",
		!strcmp(seq->buffer + seq->offset, "GGCGCGCTGATTACCAGATGATCAGTAA")
	);

	/* Sequence 3 */
	seq = sequences->values + 2;
	mu_assert("wrong id for rbsC_S_3", !strcmp(seq_get_id(seq), "rbsC_S_3"));
	mu_assert(
		"wrong sequence for rbsC_S_3",
		!strcmp(seq->buffer + seq->offset, "ATGACAACCCAGGCTGTTACTGGTCGCCG")
	);
	
	destroy_sequences(sequences);

	return 0;
}




char * fasta_tests() {
	printf("FASTA tests\n");

	mu_run_test(test_file_without_endline);
	mu_run_test(test_file_with_endline);
	mu_run_test(test_values_from_fasta);
	return 0;
}
