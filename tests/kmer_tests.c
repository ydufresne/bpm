#include "kmer_tests.h"
#include <stdio.h>
#include <stdlib.h>

#include "binary.h"
#include "kmers.h"

char * load_data2_kmers () {
	/* -- Load kmers --- */

	binary_content * content = load_from_binary("tests/data/data2.bin");
	kmer_list_t * kmers = content->kmers;

	/* Kmer 1 */
	mu_assert("Wrong first kmer.\nOffset should be eaquals to 0", 0 == kmers->values[0].offset);
	mu_assert("Wrong first kmer.\nHash should be eaquals to 4053611982428530076", 4053611982428530076 == kmers->values[0].hash);
	/*ATGACAACCCAGGCTGTTTCTGGTCGCCGCTA*/

	/* Reverse kmer 1 */
	mu_assert("Wrong first kmer.\nOffset should be eaquals to 934", 934 == kmers->values[1].offset);
	mu_assert("Wrong first kmer.\nHash should be eaquals to 14530323788978634451", 14530323788978634451U == kmers->values[1].hash);
	/*TAGCGGCGACCAGAAACAGCCTGGGTTGTCAT*/

	/* Kmer 2 */
	mu_assert("Wrong first kmer.\nOffset should be eaquals to 0", 0 == kmers->values[2].offset);
	mu_assert("Wrong first kmer.\nHash should be eaquals to 4067122781109315004", 4067122781109315004 == kmers->values[2].hash);
	/*ATGACTACCCAGGCTGTTACTGGTCGCCGTTA*/

	/* Reverse kmer 2 */
	mu_assert("Wrong first kmer.\nOffset should be eaquals to 934", 934 == kmers->values[3].offset);
	mu_assert("Wrong first kmer.\nHash should be eaquals to 13953863242833638099", 13953863242833638099U == kmers->values[3].hash);
	/*TAACGGCGACCAGTAACAGCCTGGGTAGTCAT*/

	/* Kmer 3 */
	mu_assert("Wrong first kmer.\nOffset should be eaquals to 0", 0 == kmers->values[4].offset);
	mu_assert("Wrong first kmer.\nHash should be eaquals to 4053611982227203516", 4053611982227203516 == kmers->values[4].hash);
	/*ATGACAACCCAGGCTGTTACTGGTCGCCGTTA*/

	/* Reverse kmer 3 */
	mu_assert("Wrong first kmer.\nOffset should be eaquals to 934", 934 == kmers->values[5].offset);
	mu_assert("Wrong first kmer.\nHash should be eaquals to 13953863242833641171", 13953863242833641171U == kmers->values[5].hash);
	/*TAACGGCGACCAGTAACAGCCTGGGTTGTCAT*/
	
	return 0;
}


char * kmer_tests () {
	printf("Kmer loading tests\n");

	mu_run_test(load_data2_kmers);

	return 0;
}
